<?php

class SearchUserTest extends \PHPUnit\Framework\TestCase {
  
  public function testUserFound() {
    $sc = new \App\SClient();
    $resp = $sc->SearchUser(new \App\SearchUserRequest("foo@bar.cz", "Foo", "Bar", "123"));
    
    $this->assertEquals($resp->getResult(), 0);
    $this->assertEquals($resp->getEmail(), "foo@bar.cz");
  }
  
  public function testNoUser() {
    $sc = new \App\SClient();
    $resp = $sc->SearchUser(new \App\SearchUserRequest("foo@bar.cz", "John", "Bar", "123"));
    
    $this->assertEquals($resp->getResult(), 1);
    $this->assertEquals($resp->getErrMsg(), "Nothing Found!");
  }
}