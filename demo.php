<?php

header("Content-type: text/plain");
require __DIR__ . '/vendor/autoload.php';

use \App\SClient;
use \App\SServer;
use \App\SearchUserRequest;
use \App\SearchUserResponse;

try {
  $client = new SClient();
  
  print_r(
    $client->SearchUser(new SearchUserRequest("foo@bar.cz", "Foo", "Bar", "123456789"))
  );
  
} catch (SoapFault $e) {
  var_dump($e);
} finally {
  echo "\nRaw XML Request\n".$client->__getLastRequest();
  echo "\nRaw XML Request Headers\n".$client->__getLastRequestHeaders();
  echo "\nRaw XML Response\n".$client->__getLastResponse();
  echo "\nRaw XML Response Headers\n".$client->__getLastResponseHeaders();
}

?>