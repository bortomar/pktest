<?php

namespace App;

//use \SoapClient;

class SClient extends \SoapClient {
  
  public function __construct() {
    
    parent::__construct(
      "app/PKTest.wsdl",
      [
        "classmap" => [          
          'SearchUserRequest' => 'App\SearchUserRequest',
          'SearchUserResponse' => 'App\SearchUserResponse',
        ],
        //"location" => "http://". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . "/app/SServer.php",
        "location" => "http://localhost/pktest/app/SServer.php",
        "trace" => true
      ]
    );
    
   }
}

?>  