<?php

namespace App;

require __DIR__ . '/../vendor/autoload.php';

$server = new \SoapServer(
        "./PKTest.wsdl",
        [
          "classmap" => [          
            'SearchUserRequest' => 'App\SearchUserRequest',
            'SearchUserResponse' => 'App\SearchUserResponse',
          ]
        ]
);
$server->setClass('App\SServer');
$server->handle();
    
class SServer {
  
  public function SearchUser(SearchUserRequest $req) {
    
    try {  
    
      $db = new Database('user.db');      
      $s = $db->getUser($req);
      
      if (!$s) {
        throw new \Exception("Nothing Found!");
      }
      
      $s->setResult(0);        
      return $s;
    
    } catch (\Exception $e) {
      return new SearchUserResponse(1, $e->getMessage(), null, "", "", "", "");
    }
  }

}

