<?php
namespace App;
class SearchUserRequest
{

    /**
     * @var String200 $Email
     */
    protected $Email = null;

    /**
     * @var String200 $FirstName
     */
    protected $FirstName = null;

    /**
     * @var String200 $Surname
     */
    protected $Surname = null;

    /**
     * @var String200 $Phone
     */
    protected $Phone = null;

    /**
     * @param String200 $Email
     * @param String200 $FirstName
     * @param String200 $Surname
     * @param String200 $Phone
     */
    public function __construct($Email, $FirstName, $Surname, $Phone)
    {
      $this->Email = $Email;
      $this->FirstName = $FirstName;
      $this->Surname = $Surname;
      $this->Phone = $Phone;
    }

    /**
     * @return String200
     */
    public function getEmail()
    {
      return $this->Email;
    }

    /**
     * @param String200 $Email
     * @return SearchUserRequest
     */
    public function setEmail($Email)
    {
      $this->Email = $Email;
      return $this;
    }

    /**
     * @return String200
     */
    public function getFirstName()
    {
      return $this->FirstName;
    }

    /**
     * @param String200 $FirstName
     * @return SearchUserRequest
     */
    public function setFirstName($FirstName)
    {
      $this->FirstName = $FirstName;
      return $this;
    }

    /**
     * @return String200
     */
    public function getSurname()
    {
      return $this->Surname;
    }

    /**
     * @param String200 $Surname
     * @return SearchUserRequest
     */
    public function setSurname($Surname)
    {
      $this->Surname = $Surname;
      return $this;
    }

    /**
     * @return String200
     */
    public function getPhone()
    {
      return $this->Phone;
    }

    /**
     * @param String200 $Phone
     * @return SearchUserRequest
     */
    public function setPhone($Phone)
    {
      $this->Phone = $Phone;
      return $this;
    }

}
