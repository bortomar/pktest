<?php

  header("Content-type: text/plain");
  
  // load soap request template from file
  $soapReqTemplate = new DOMDocument();
  $soapReqTemplate->load("curl-req-template.xml");
  $soapReqTemplate->createAttributeNS("urn:Test", "tns:req");
  $soapReqTemplate->getElementsByTagNameNS('http://schemas.xmlsoap.org/soap/envelope/', 'Body')[0]
    ->appendChild($soapReqTemplate->createElementNS("urn:PKTest", "SearchUserRequest"))
    ->appendChild($soapReqTemplate->createElementNS("urn:PKTest", "Email", "foo@bar.cz"));
  print $soapReqTemplate->saveXML();
  
  
  $headers = array(
      "Content-type: text/xml;charset=\"utf-8\"",
      "Accept: text/xml",
      "Cache-Control: no-cache",
      "Pragma: no-cache",
      "SOAPAction: ", 
      "Content-length: ".strlen($soapReqTemplate->saveXML()),
  );
  
  // PHP cURL  for https connection with auth
  $ch = curl_init();
  //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
  curl_setopt($ch, CURLOPT_URL, "http://localhost/pktest/app/SServer.php");
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  //curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
  //curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
  curl_setopt($ch, CURLOPT_TIMEOUT, 10);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $soapReqTemplate->saveXML()); // the SOAP request
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

  // converting
  $response = curl_exec($ch); 
  curl_close($ch);
  
  echo $response;