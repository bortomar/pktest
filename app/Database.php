<?php 

namespace App;

class Database  {
   
  private $dbConnection;
  
  public function __construct($filename) {
    try {
      $this->dbConnection = new \PDO('sqlite:./'.$filename);
    }  catch (\Exception $e) {
      throw $e;
    }
  }
  
  public function getUser(SearchUserRequest $req) {
    
    $r = [];
    foreach(get_class_methods(get_class($req)) as $v) {
        if (preg_match('/get(.+)/', $v, $m) && $req->{$m[0]}()) { 
          array_push($r, $m[1].' LIKE \'%'.$req->{$m[0]}().'%\'');
        }
    }
    
    if (count($r))
      $where = " WHERE ".implode(' AND ', $r);    
    
    try {      
      $r = $this->dbConnection->prepare("SELECT Id, Firstname, Surname, Email, Phone FROM user".(isset($where) ? $where : "")); 
      $r->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, 'App\SearchUserResponse', array_fill(0, 7, ""));      
      
      $r->execute();
      $s = $r->fetch();      
      
      return $s;
      
    } catch (\Exception $e) {
      throw $e;
    }
    
  }
  
}
