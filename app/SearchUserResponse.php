<?php
namespace App;
class SearchUserResponse
{

    /**
     * @var int $Result
     */
    protected $Result = null;

    /**
     * @var String200 $ErrMsg
     */
    protected $ErrMsg = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var String200 $Email
     */
    protected $Email = null;

    /**
     * @var String200 $Firstname
     */
    protected $Firstname = null;

    /**
     * @var String200 $Surname
     */
    protected $Surname = null;

    /**
     * @var String200 $Phone
     */
    protected $Phone = null;

    /**
     * @param int $Result
     * @param String200 $ErrMsg
     * @param int $Id
     * @param String200 $Email
     * @param String200 $Firstname
     * @param String200 $Surname
     * @param String200 $Phone
     */
    public function __construct($Result, $ErrMsg, $Id, $Email, $Firstname, $Surname, $Phone)
    {
      $this->Result = $Result;
      $this->ErrMsg = $ErrMsg;
      $this->Id = $Id;
      $this->Email = $Email;
      $this->Firstname = $Firstname;
      $this->Surname = $Surname;
      $this->Phone = $Phone;
    }

    /**
     * @return int
     */
    public function getResult()
    {
      return $this->Result;
    }

    /**
     * @param int $Result
     * @return SearchUserResponse
     */
    public function setResult($Result)
    {
      $this->Result = $Result;
      return $this;
    }

    /**
     * @return String200
     */
    public function getErrMsg()
    {
      return $this->ErrMsg;
    }

    /**
     * @param String200 $ErrMsg
     * @return SearchUserResponse
     */
    public function setErrMsg($ErrMsg)
    {
      $this->ErrMsg = $ErrMsg;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return SearchUserResponse
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return String200
     */
    public function getEmail()
    {
      return $this->Email;
    }

    /**
     * @param String200 $Email
     * @return SearchUserResponse
     */
    public function setEmail($Email)
    {
      $this->Email = $Email;
      return $this;
    }

    /**
     * @return String200
     */
    public function getFirstname()
    {
      return $this->Firstname;
    }

    /**
     * @param String200 $Firstname
     * @return SearchUserResponse
     */
    public function setFirstname($Firstname)
    {
      $this->Firstname = $Firstname;
      return $this;
    }

    /**
     * @return String200
     */
    public function getSurname()
    {
      return $this->Surname;
    }

    /**
     * @param String200 $Surname
     * @return SearchUserResponse
     */
    public function setSurname($Surname)
    {
      $this->Surname = $Surname;
      return $this;
    }

    /**
     * @return String200
     */
    public function getPhone()
    {
      return $this->Phone;
    }

    /**
     * @param String200 $Phone
     * @return SearchUserResponse
     */
    public function setPhone($Phone)
    {
      $this->Phone = $Phone;
      return $this;
    }

    public function __set($prop, $value) {
      $this->$prop = $value;
    }
}
